// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.frontend

import kotlinx.browser.document
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.dom.addClass
import kotlinx.dom.hasClass
import kotlinx.dom.removeClass
import kotlinx.html.*
import org.w3c.dom.*
import kotlin.coroutines.CoroutineContext

private class Application : CoroutineScope {
    override val coroutineContext: CoroutineContext = Job()

    suspend fun start() {
        // enable the drop-down menus in mobile view
        (document.getElementById("header-btn") as HTMLDivElement?)?.onclick = { _ ->
            fun Element.toggleClass(clazz: String) {
                if (hasClass(clazz))
                    removeClass(clazz)
                else
                    addClass(clazz)
            }
            document.getElementById("header-menu")?.toggleClass("active")
            document.getElementsByClassName("nav-btn").asList().forEach { it.toggleClass("active") }
        }

        // submit the search input (package name) on pressing enter
        (document.getElementById("searchquery") as HTMLInputElement?)?.onkeyup = { event ->
            if (event.keyCode == 13) {
                GlobalScope.launch { DOMManipulation.performSearchQuery() }
            }
        }

        (document.getElementById("querySubmit") as HTMLButtonElement?)?.onclick = { _ ->
            GlobalScope.launch { DOMManipulation.performSearchQuery() }
        }
        (document.getElementById("fetchAllTopicButton") as HTMLButtonElement?)?.onclick = { _ ->
            GlobalScope.launch { DOMManipulation.fetchAllTopics() }
        }
        (document.getElementById("fetchTopicButton") as HTMLButtonElement?)?.onclick = { _ ->
            GlobalScope.launch { DOMManipulation.fetchTopics() }
        }

        DOMManipulation.fillVersionContainer()
        DOMManipulation.fetchTopics()
    }
}

/**
 * The main method initializing the replacements and input elements on the page.
 */
suspend fun main() = Application().start()

/**
 * Generate the stylized [P] showing the TeX logo.
 */
fun P.tex() = span("tex") {
    +"T"
    sub {
        +"e"
    }
    +"X"
}
