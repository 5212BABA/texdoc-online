// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline

import io.ktor.application.*
import io.ktor.http.content.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.*
import kotlinx.html.*
import java.io.File

/**
 * Sets up routing to serve all files from [folder] and when not finding anything
 * falling back to files available from [resourcePackage].
 *
 * This uses internal commands from the `StaticContent.kt` file and may not be the
 * most stable option.
 */
fun Route.filesWithResourceFallback(folder: File, resourcePackage: String) {
    val pathParameterName = "static-content-path-parameter"
    val dir = when (staticRootFolder) {
        null -> folder
        else -> staticRootFolder!!.resolve(folder)
    }
    val packageName = when (staticBasePackage) {
        null -> resourcePackage
        else -> "$staticBasePackage.$resourcePackage"
    }
    get("{$pathParameterName...}") {
        // try files first
        val fileRelPath = call.parameters.getAll(pathParameterName)?.joinToString(File.separator) ?: return@get
        val file = dir.combineSafe(fileRelPath)
        if (file.isFile) {
            call.respond(LocalFileContent(file))
            return@get
        }

        // fall back to resources
        val resourceRelPath = call.parameters.getAll(pathParameterName)?.joinToString(File.separator) ?: return@get
        val content = call.resolveResource(resourceRelPath, packageName)
        if (content != null)
            call.respond(content)
    }
}

/**
 * Generate the stylized [P] showing the TeX logo.
 */
fun <T> TagConsumer<T>.tex(): T = SPAN(mapOf("class" to "tex"), this)
    .visitAndFinalize(this) {
        +"T"
        sub {
            +"e"
        }
        +"X"
    }
