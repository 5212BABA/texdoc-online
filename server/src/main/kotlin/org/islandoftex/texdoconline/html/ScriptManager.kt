// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.html

import java.io.File
import java.security.MessageDigest
import java.util.Base64

/**
 * Manager for script resources in the web application.
 */
object ScriptManager {
    /**
     * Provide a cache for already computed resource hashes as they will not
     * change within one execution.
     */
    private val cache = mutableMapOf<String, String>()

    /**
     * Compute a valid hash string accepted by the `integrity` feature of the
     * `script` tag.
     *
     * Currently, this returns a sha 512 sum but this detail should not be
     * relied upon.
     */
    @Suppress("MagicNumber")
    fun getIntegrityHashForScript(path: String): String = cache[path]
        ?: ("sha512-" + MessageDigest.getInstance("SHA-512")
            .run {
                val streamSource = File(path).takeIf { it.exists() }?.inputStream()
                    ?: ScriptManager::class.java
                        .getResourceAsStream("/org/islandoftex/texdoconline/$path")
                    ?: throw IllegalArgumentException("Invalid path for integrity hashing")
                streamSource.use { stream ->
                    val buffer = ByteArray(8192)
                    var read: Int
                    while (stream.read(buffer).also { read = it } > 0) {
                        update(buffer, 0, read)
                    }
                }
                digest()
            }
            .let { Base64.getEncoder().encodeToString(it) })
            .also { cache[path] = it }
}
