// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.build

object Versions {
    // plugin dependencies
    const val detekt = "1.19.0"
    const val dokka = "1.6.10"
    const val kotlin = "1.6.10"
    const val shadow = "7.1.2"
    const val spotless = "6.3.0"
    const val spotlessChangelog = "2.4.0"
    const val versionsPlugin = "0.42.0"

    // non-plugin dependencies
    const val clikt = "3.4.0"
    const val coroutines = "1.5.0"
    const val ctanapi = "0.1.0"
    const val datetime = "0.3.2"
    const val html = "0.7.2"
    const val kotest = "5.2.1"
    const val ktor = "1.6.8"
    const val serialization = "1.3.2"
    const val slf4j = "1.7.36"
    const val texdocapi = "1.0.0"
}
