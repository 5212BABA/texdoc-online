# TeXdoc online

![Language: Kotlin](https://img.shields.io/badge/Language-Kotlin-blue.svg?style=flat-square)
![Minimum JRE: 8.0](https://img.shields.io/badge/Minimum_JRE-8.0-blue.svg?style=flat-square)
![Current version](https://img.shields.io/badge/dynamic/json.svg?color=blue&label=Latest%20release&query=%24.0.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F19876226%2Frepository%2Ftags&style=flat-square)

## Introduction

TeXdoc online is an online TeX and LaTeX documentation lookup system developed and maintained by the Island of TeX. The project provides a RESTful API and a self-updating Docker container.

The package documentation lookup relies on the Island's [TeXdoc API](https://gitlab.com/islandoftex/libraries/texdoc-api). For topic and package listings, as well as recommendations, the system relies on the CTAN JSON API, properly cached and updated twice a day to save external queries.

Details on the provided RESTful API and deployment may be found on our Wiki:

* [API details](openapi.yaml)
* [Deploying your instance of TeXdoc online](https://gitlab.com/islandoftex/images/texdoc-online/-/wikis/Deploying-your-instance-of-TeXdoc-online)
* [Configuration options](https://gitlab.com/islandoftex/images/texdoc-online/-/wikis/Configuration-options)

To cut a long story short: You can run the application from the CI-built JAR files, the Docker container provided in our [registry](https://gitlab.com/islandoftex/images/texdoc-online/container_registry) and for real deployment using Docker compose built on the `docker-compose.yml` file in the repo.

## License

This application is licensed under the [New BSD License](https://opensource.org/licenses/BSD-3-Clause). Please note that the New BSD License has been verified as a GPL-compatible free software license by the [Free Software Foundation](http://www.fsf.org/), and has been vetted as an open source license by the [Open Source Initiative](http://www.opensource.org/).

## The team

TeXdoc online is brought to you by the Island of TeX. If you want to support TeX development by a donation, the best way to do this is donating to the [TeX Users Group](https://www.tug.org/donate.html).

