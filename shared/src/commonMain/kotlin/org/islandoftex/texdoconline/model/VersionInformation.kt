// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.model

import kotlinx.serialization.Serializable

/**
 * Container for all relevant versions texdoc-online needs to consider.
 */
@Serializable
public data class VersionInformation(
    /**
     * The API version as specified in the server component.
     */
    val api: String,
    /**
     * The version of texdoc, the tool executed by texdoc online.
     */
    val texdoc: String,
    /**
     * The last update of the underlying TeX Live installation given by
     * the file modification date of the tlpdb.
     */
    val tlpdb: String
)
