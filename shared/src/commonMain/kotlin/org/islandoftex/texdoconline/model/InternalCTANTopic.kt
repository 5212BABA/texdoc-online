// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.model

import kotlinx.serialization.Serializable

/**
 * This represents a topic on CTAN. Topics are a way to group packages by their
 * purpose.
 */
@Serializable
public data class InternalCTANTopic(
    /**
     * The identifier of the topic. This identifier is used within the web interface as well.
     */
    val key: String,
    /**
     * A short description of the topic and its purpose.
     */
    val details: String,
    /**
     * The list of packages belonging to the topic.
     */
    val packages: List<String> = listOf()
)
